#!/bin/sh

#PBS -N DSS
#PBS -q copperhead
#PBS -l nodes=1:ppn=2
#PBS -l mem=70000mb
#PBS -l walltime=30:00:00

module load R/3.2.3

cd $PBS_O_WORKDIR

R CMD BATCH DSS_A17vsLSS_CHH.R
