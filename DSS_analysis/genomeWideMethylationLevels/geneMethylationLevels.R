
install.packages("data.table", repos = "http://cran.us.r-project.org")
install.packages("ggplot2",repos = "http://cran.us.r-project.org")
library(ggplot2)
library(data.table)
library(tools)

#Peek at a sampling of the first chromosome methylation distribution
#reading in samples
samples = list()
samples = commandArgs(trailingOnly=TRUE)

v = c("chr","pos","numCyto","numMeth")
lociList = list()

for (sampleNum in 1:length(samples)) {
  tempTable = read.table(file=paste("../data/",samples[sampleNum], sep=""),header = TRUE,quote = "",col.names = v, colClasses = c("character","integer","integer","integer"))
  lociList[[sampleNum]] = tempTable
  print(c("Completed importing sample: ", samples[sampleNum]), quote = FALSE)
}

#variables
upstreamDistance = 1000
downstreamDistance = 1000
geneWidth = 1000
genomeAnnotation = "../annotationFile/M_truncatula_Aug_2013.bed.gz"
sampleName = file_path_sans_ext(samples[[1]], compression = TRUE)
print("Variable assignment complete",quote = FALSE)

#importing the non-TE bed file of Medicago genes
bedColumns=c("chrom","chromStart","chromEnd","name","score","strand","thickStart","thickEnd","itemRgb","blockCount","blockSizes","blockStarts")
genes = read.table(file=genomeAnnotation, header = FALSE, sep = "\t", quote = "", stringsAsFactors = FALSE, col.names = bedColumns)
print("Gene annotation importing complete",quote = FALSE)

#using only isoform 1. Seems like it would be confusing to double-up by having each isoform, since many upstream regions would overlap
genesNoIso = genes[grep(".1", genes$name, fixed= TRUE),]
genesNoIso = data.table(genesNoIso)
print("Isoform 1 isolation complete",quote = FALSE)

#Plotting the methylation levels, should be bimodal, 0 or 1
methylationLevels = double()
for (i in 1:length(lociList)) {
  tempDF = data.frame(chr=character(),pos=integer(),numCyto=integer(),numMeth=integer(),stringsAsFactors = FALSE)
  tempDF = lociList[[i]]
  methylationLevels = c(methylationLevels,(tempDF$numMeth/tempDF$numCyto))
}
methylationHistogram = data.frame(methylationLevels, stringsAsFactors = FALSE)
qplot(methylationHistogram$methylationLevels, 
      geom="histogram",
      main = "Methylation across genome",
      xlab = "Percent methylation",
      ylab = "cytosine counts")
ggsave(paste(sampleName,"_histogram.png",sep=""), path = "./output", scale = 1, width = 4, height = 3, units = "in", dpi = 300)
print("Methylation histogram complete",quote = FALSE)

#######################
#######UPSTREAM########
#######################
#Averaging all of the upstream regions for every non-TE gene in rice
#create arrays to hold the cytosine and methylation counts for the 1000 bp upstream
upstreamCytosineArray = c(rep(0, each=upstreamDistance))
upstreamMethylationArray = c(rep(0, each=upstreamDistance))

#This outer loop will go through each chromosome
for (chromosome in unique(genesNoIso$chrom)) {
  
  #subset the methylation data to one chromosome at a time
  chrLociList = list()
  for (i in 1:length(lociList)) {
    chrLociList[[i]] = (subset(lociList[[i]], chr == chromosome))
  }
  
  #subset the genome data to one chromosome at a time
  genesNoIsoChrom1 = subset(genesNoIso, chrom == chromosome)
  
  #creating vectors to hold the upstream start and end positions
  upstreamStart = vector()
  upstreamEnd = vector()
  
  #filling vectors with the upstream start and end sites, being mindful of whether the gene is on the positive or negative strand
  for(row in 1:nrow(genesNoIsoChrom1)){
    if(genesNoIsoChrom1$strand[row] == "-"){
      upstreamStart = c(upstreamStart, genesNoIsoChrom1$chromEnd[row] + upstreamDistance)
      upstreamEnd = c(upstreamEnd, genesNoIsoChrom1$chromEnd[row])
    }else{
      upstreamStart = c(upstreamStart, genesNoIsoChrom1$chromStart[row] - upstreamDistance)
      upstreamEnd = c(upstreamEnd, genesNoIsoChrom1$chromStart[row])
    }
  }
  
  #creating empty dataframes to hold subsets
  tempDFList = list()
  for (i in 1:length(chrLociList)) {
    tempDFList[[i]] = data.frame(chr=character(),pos=integer(),numCyto=integer(),numMeth=integer(),stringsAsFactors = FALSE)
  }
  
  #looping through each upstream region, dependent on whether gene is on negative or positive strand
  for(index in 1:length(upstreamStart)){
    if(upstreamEnd[index]-upstreamStart[index] > 0) {
      #then the gene is on positive strand
      for (i in 1:length(tempDFList)) {
        tempDFList[[i]] = (subset(chrLociList[[i]], pos >= upstreamStart[index] & pos <= upstreamEnd[index]))
        tempDF = data.frame(chr=character(),pos=integer(),numCyto=integer(),numMeth=integer(),stringsAsFactors = FALSE)
        tempDF = tempDFList[[i]]
        #loop through each dataframe holding the methylation/cytosine data, adding the data to the array
        for(indexTwo in 1:length(tempDF$numCyto)){
          upstreamCytosineArray[tempDF$pos[indexTwo] - upstreamStart[index]] = upstreamCytosineArray[tempDF$pos[indexTwo] - upstreamStart[index]] + tempDF$numCyto[indexTwo]
          upstreamMethylationArray[tempDF$pos[indexTwo] - upstreamStart[index]] = upstreamMethylationArray[tempDF$pos[indexTwo] - upstreamStart[index]] + tempDF$numMeth[indexTwo]
        }
      }
    }
    else{
      #gene is on negative strand
      for (i in 1:length(tempDFList)) {
        tempDFList[[i]] = (subset(chrLociList[[i]], pos <= upstreamStart[index] & pos >= upstreamEnd[index]))
        tempDF = data.frame(chr=character(),pos=integer(),numCyto=integer(),numMeth=integer(),stringsAsFactors = FALSE)
        tempDF = tempDFList[[i]]
        #loop through each dataframe holding the methylation/cytosine data, adding the data to the array
        for(indexTwo in 1:length(tempDF$numCyto)){
          upstreamCytosineArray[upstreamStart[index] - tempDF$pos[indexTwo]] = upstreamCytosineArray[upstreamStart[index] - tempDF$pos[indexTwo]] + tempDF$numCyto[indexTwo]
          upstreamMethylationArray[upstreamStart[index] - tempDF$pos[indexTwo]] = upstreamMethylationArray[upstreamStart[index] - tempDF$pos[indexTwo]] + tempDF$numMeth[indexTwo]
        }
      }
    }
    cat(c(chromosome,((index/length(upstreamStart)) * 100), "% complete - upstream\n"))
  }
}
upstreamPlot = upstreamMethylationArray/upstreamCytosineArray
upstreamPlotDF = data.frame(upstreamPlot, stringsAsFactors = FALSE)

#######################
######DOWNSTREAM#######
#######################
#Averaging all of the downstream regions for every non-TE gene in rice
#create arrays to hold the cytosine and methylation counts for the 1000 bp downstream
downstreamCytosineArray = c(rep(0, each=downstreamDistance))
downstreamMethylationArray = c(rep(0, each=downstreamDistance))

#This outer loop will go through each chromosome
for (chromosome in unique(genesNoIso$chrom)) {
  
  #subset the methylation data to one chromosome at a time
  chrLociList = list()
  for (i in 1:length(lociList)) {
    chrLociList[[i]] = (subset(lociList[[i]], chr == chromosome))
  }
  
  #subset the genome data to one chromosome at a time
  genesNoIsoChrom1 = subset(genesNoIso, chrom == chromosome)
  
  #creating vectors to hold the downstream start and end positions
  downstreamStart = vector()
  downstreamEnd = vector()
  
  #filling vectors with the downstream start and end sites, being mindful of whether the gene is on the positive or negative strand
  for(row in 1:nrow(genesNoIsoChrom1)){
    if(genesNoIsoChrom1$strand[row] == "-"){
      downstreamStart = c(downstreamStart, genesNoIsoChrom1$chromStart[row])
      downstreamEnd = c(downstreamEnd, genesNoIsoChrom1$chromStart[row] - downstreamDistance)
    }else{
      downstreamStart = c(downstreamStart, genesNoIsoChrom1$chromEnd[row])
      downstreamEnd = c(downstreamEnd, genesNoIsoChrom1$chromEnd[row] + downstreamDistance)
    }
  }
  
  #creating empty dataframes to hold subsets
  tempDFList = list()
  for (i in 1:length(chrLociList)) {
    tempDFList[[i]] = data.frame(chr=character(),pos=integer(),numCyto=integer(),numMeth=integer(),stringsAsFactors = FALSE)
  }
  
  #looping through each downstream region, dependent on whether gene is on negative or positive strand
  for(index in 1:length(downstreamStart)){
    if(downstreamEnd[index]-downstreamStart[index] > 0) {
      #then the gene is on positive strand
      for (i in 1:length(tempDFList)) {  
        tempDFList[[i]] = (subset(chrLociList[[i]], pos >= downstreamStart[index] & pos <= downstreamEnd[index] ))
        tempDF = data.frame(chr=character(),pos=integer(),numCyto=integer(),numMeth=integer(),stringsAsFactors = FALSE)
        tempDF = tempDFList[[i]]
        #loop through each dataframe holding the methylation/cytosine data, adding the data to the array
        for(indexTwo in 1:length(tempDF$numCyto)){
          downstreamCytosineArray[tempDF$pos[indexTwo] - downstreamStart[index]] = downstreamCytosineArray[tempDF$pos[indexTwo] - downstreamStart[index]] + tempDF$numCyto[indexTwo]
          downstreamMethylationArray[tempDF$pos[indexTwo] - downstreamStart[index]] = downstreamMethylationArray[tempDF$pos[indexTwo] - downstreamStart[index]] + tempDF$numMeth[indexTwo]
        }
      }
    }
    else{
      #gene is on negative strand
      for (i in 1:length(tempDFList)) {
        tempDFList[[i]] = (subset(chrLociList[[i]], pos <= downstreamStart[index] & pos >= downstreamEnd[index] ))
        tempDF = data.frame(chr=character(),pos=integer(),numCyto=integer(),numMeth=integer(),stringsAsFactors = FALSE)
        tempDF = tempDFList[[i]]
        #loop through each dataframe holding the methylation/cytosine data, adding the data to the array
        for(indexTwo in 1:length(tempDF$numCyto)){
          downstreamCytosineArray[downstreamStart[index] - tempDF$pos[indexTwo]] = downstreamCytosineArray[downstreamStart[index] - tempDF$pos[indexTwo]] + tempDF$numCyto[indexTwo]
          downstreamMethylationArray[downstreamStart[index] - tempDF$pos[indexTwo]] = downstreamMethylationArray[downstreamStart[index] - tempDF$pos[indexTwo]] + tempDF$numMeth[indexTwo]
        }
      }
    }
    cat(c(chromosome,((index/length(downstreamStart)) * 100), "% complete-downstream\n"))
  }
}
downstreamPlot = downstreamMethylationArray/downstreamCytosineArray
downstreamPlotDF = data.frame(downstreamPlot, stringsAsFactors = FALSE)

#######################
#########GENE##########
#######################
#Averaging all of the methylation in the gene (UTR included) for every non-TE gene in rice
#create arrays to hold the cytosine and methylation counts for the gene
geneCytosineArray = c(rep(0, each=geneWidth))
geneMethylationArray = c(rep(0, each=geneWidth))

#This outer loop will go through each chromosome
for (chromosome in unique(genesNoIso$chrom)) {
  
  #subset the methylation data to one chromosome at a time
  chrLociList = list()
  for (i in 1:length(lociList)) {
    chrLociList[[i]] = (subset(lociList[[i]], chr == chromosome))
    chrLociList[[i]] = data.table(as.matrix(chrLociList[[i]][,-1]))
  }
  
  #subset the genome data to one chromosome at a time
  genesNoIsoChrom1 = subset(genesNoIso, chrom == chromosome)
  
  #Looping through all of the genes on this chromosome
  for(gene in 1:nrow(genesNoIsoChrom1)){
    #Calculate number of blocks with coding sequence
    codingBlock = vector()
    for (blocks in 1:genesNoIsoChrom1[gene]$blockCount) {
      if(
        #if the block falls between the thick start and thickend -= (=) =-
        (genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][blocks]) >= genesNoIsoChrom1[gene]$thickStart & 
         genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][blocks]) + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockSizes, ",")[[1]][blocks]) <= genesNoIsoChrom1[gene]$thickEnd) |
        #if the thickstart falls after the block starts but before the block ends (-=) = =-
        (genesNoIsoChrom1[gene]$thickStart >= genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][blocks]) &
         genesNoIsoChrom1[gene]$thickStart <=  genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][blocks]) + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockSizes, ",")[[1]][blocks])) |
        #if the thickend falls after the block starts but before the block ends -= = (=-)
        (genesNoIsoChrom1[gene]$thickEnd >= genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][blocks]) &
         genesNoIsoChrom1[gene]$thickEnd <=  genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][blocks]) + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockSizes, ",")[[1]][blocks]))){
        codingBlock[blocks] = TRUE
      }
      else{
        codingBlock[blocks] = FALSE
      }
    }
    
    #Determine the length of the coding sequence only
    firstBlock = min(which(codingBlock == TRUE))
    lastBlock = max(which(codingBlock == TRUE))
    geneLength = 0
    for (codeBlock in 1:length(codingBlock)) {
      if(codingBlock[codeBlock]){
        #first coding block of many (-=) = =-
        if(firstBlock == codeBlock & firstBlock != lastBlock){
          geneLength = geneLength + 
            ((genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][codeBlock]) + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockSizes, ",")[[1]][codeBlock])) - genesNoIsoChrom1[gene]$thickStart)
        }
        #last coding block -= = (=-)
        else if(lastBlock == codeBlock & lastBlock != firstBlock){
          geneLength = geneLength + 
            (genesNoIsoChrom1[gene]$thickEnd - (genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][codeBlock])))
        }
        #coding block in the middle -= (=) =-
        else if(firstBlock != codeBlock & lastBlock != codeBlock){
          geneLength = geneLength + 
            (as.numeric(strsplit(genesNoIsoChrom1[gene]$blockSizes, ",")[[1]][codeBlock]))
        }
        #first coding block of only one coding block (-=-)
        else if(firstBlock == codeBlock & firstBlock == lastBlock){
          geneLength = geneLength + (genesNoIsoChrom1[gene]$thickEnd - genesNoIsoChrom1[gene]$thickStart)
        }
      }
    }
    
    #Determine the number of introns in the coding region and what each of their lengths is
    intronLength = vector()
    if(genesNoIsoChrom1[gene]$blockCount != 1) {
      for (intron in 1:(genesNoIsoChrom1[gene]$blockCount - 1)) {
        if(genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][intron + 1]) <= genesNoIsoChrom1[gene]$thickEnd &
           (genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][intron]) + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockSizes, ",")[[1]][intron])) >= genesNoIsoChrom1[gene]$thickStart)
        {
          intronLength[intron] = as.numeric((genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][intron + 1]) - 
                                               (genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][intron]) + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockSizes, ",")[[1]][intron]))))
        }
        else{
          intronLength[intron] = 0
        }
      }
    }
    
    if(genesNoIsoChrom1$strand[gene] == "+"){
      for(biologicalRep in 1:length(chrLociList)){
        geneBlocks = data.table(pos=integer(),numCyto=integer(),numMeth=integer(),numBlocks=integer(),stringsAsFactors = FALSE)
        for(block in 1:genesNoIsoChrom1[gene]$blockCount){
          if(codingBlock[block]){
            tempDT = data.table(pos=integer(),numCyto=integer(),numMeth=integer(),stringsAsFactors = FALSE)
            tempDT = subset(chrLociList[[biologicalRep]], 
                              pos >= genesNoIsoChrom1[gene]$thickStart & 
                              pos <= genesNoIsoChrom1[gene]$thickEnd & 
                              pos >= genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][block]) & 
                              pos <= genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][block]) + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockSizes, ",")[[1]][block]))
            tempDT2 = data.table(numBlocks = rep(block,nrow(tempDT)))
            geneBlocks = rbind(geneBlocks, cbind(tempDT,tempDT2))
          }
        }
        
        #Lots of logic to figure out the percentage of the way through just the coding sequence
        if(length(geneBlocks$numCyto) > 0){
          for(rows in 1:length(geneBlocks$numCyto)){
            intronCounterLength = 0
            if(geneBlocks$numBlocks[rows] > 1){
              for (introns in 1:(geneBlocks$numBlocks[rows] - 1)) {
                if(codingBlock[introns]){
                  intronCounterLength = intronCounterLength + intronLength[introns]
                }
              }
            }
            indexArray = ceiling(((geneBlocks$pos[rows] - genesNoIsoChrom1[gene]$thickStart - intronCounterLength)/geneLength)*geneWidth)
            geneCytosineArray[indexArray] = geneCytosineArray[indexArray] + geneBlocks$numCyto[rows]
            geneMethylationArray[indexArray] = geneMethylationArray[indexArray] + geneBlocks$numMeth[rows]
          }
        }
      }
    }
    else{
      #Negative
      for(biologicalRep in 1:length(chrLociList)){
        geneBlocks = data.table(pos=integer(),numCyto=integer(),numMeth=integer(),numBlocks=integer(),stringsAsFactors = FALSE)
        for(block in 1:genesNoIsoChrom1[gene]$blockCount){
          if(codingBlock[block]){
            tempDT = data.table(pos=integer(),numCyto=integer(),numMeth=integer(),stringsAsFactors = FALSE)
            tempDT = subset(chrLociList[[biologicalRep]], 
                              pos >= genesNoIsoChrom1[gene]$thickStart & 
                              pos <= genesNoIsoChrom1[gene]$thickEnd & 
                              pos >= genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][block]) & 
                              pos <= genesNoIsoChrom1[gene]$chromStart + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockStarts, ",")[[1]][block]) + as.numeric(strsplit(genesNoIsoChrom1[gene]$blockSizes, ",")[[1]][block]))
            tempDT2 = data.table(numBlocks = rep(block,nrow(tempDT)))
            geneBlocks = rbind(geneBlocks, cbind(tempDT,tempDT2))
          }
        }
        #if methylation was found within the current coding block
        if(length(geneBlocks$numCyto) > 0){
          #loop through each row of the geneBlocks data table, containing rows of pos/cytosine/methylation/exon
          for(rows in 1:length(geneBlocks$numCyto)){
            intronCounterLength = 0
            #Check if it is the first exon, if it is not then go through the loop (remember that this is negative, so the last exon is really the first exon if read from left to right)
            if(geneBlocks$numBlocks[rows] < length(codingBlock)) {
              for (introns in (length(codingBlock)-1):(geneBlocks$numBlocks[rows])) {
                if(codingBlock[introns]){
                  intronCounterLength = intronCounterLength + intronLength[introns]
                }
              }
            }
            indexArray = ceiling(((genesNoIsoChrom1[gene]$thickEnd - geneBlocks$pos[rows] - intronCounterLength)/geneLength)*geneWidth)
            geneCytosineArray[indexArray] = geneCytosineArray[indexArray] + geneBlocks$numCyto[rows]
            geneMethylationArray[indexArray] = geneMethylationArray[indexArray] + geneBlocks$numMeth[rows]
          }
        }
      }
    }
    cat(c(chromosome,round(((gene/nrow(genesNoIsoChrom1)) * 100),digits=2), "% complete-gene\n"))
  }
}
genePlot = geneMethylationArray/geneCytosineArray
genePlotDF = data.frame(genePlot, stringsAsFactors = FALSE)
print("Analysis complete",quote = FALSE)

#Saving the upstream, downstream, and gene methylation levels as a table
allMethylationLevels = data.frame(upstreamMethylationArray,upstreamCytosineArray,geneMethylationArray,geneCytosineArray,downstreamMethylationArray,downstreamCytosineArray, stringsAsFactors = FALSE)
write.table(allMethylationLevels, paste("./output/",sampleName,".txt",sep=""), quote = FALSE, sep ="\t", row.names = FALSE)
print("Saving methylation levels table complete",quote = FALSE)

#Plotting the upstream, gene, and downstream together
allPlots = c(upstreamPlotDF$upstreamPlot, genePlotDF$genePlot, downstreamPlotDF$downstreamPlot)
allPlotsDF = data.frame(allPlots, stringsAsFactors = FALSE)
ggplot(data = allPlotsDF,
       aes(x=1:length(allPlots), y = allPlotsDF$allPlots)) +
  geom_line() +
  labs(x = "upstream | gene | downstream", y = "percent methylation", title = "Methylation across genes")
ggsave(paste(sampleName,"_plot.png", sep=""), path="./output", scale = 1, width = 6, height = 3, units = "in", dpi = 300)
print("Saving plot of methylation complete",quote = FALSE)
