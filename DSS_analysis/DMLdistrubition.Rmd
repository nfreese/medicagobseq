---
title: "DMLdistrubition"
author: "Nowlan"
date: "1/30/2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Pulling out the differentially methylated loci upstream, within, and downstream of the Medtr4g070970 gene of interest

```{r}
if(!require(dplyr)) install.packages("dplyr",repos = "http://cran.us.r-project.org")
library(dplyr)

#how far upstream/downstream to pull the DML information of Medtr4g070970
upstream = 10000
downstream = 10000

#position of the SUNN, CLV1, Medtr4g070970 gene that Dr. Frugoli is interested in
start = 26733659
end = 26737323
chromosome = "Chr4"

CHG_dml = read.table(file = "results/A17vLSS_DSS_DML_CHG.txt", header = TRUE, sep = "\t", quote = "")
CHG_SUNNup = filter(CHG_dml,chr==chromosome & between(pos,(start-upstream),start))
CHG_SUNN = filter(CHG_dml,chr==chromosome & between(pos,start,end))
CHG_SUNNdown = filter(CHG_dml,chr==chromosome & between(pos,end,(end+downstream)))

CHH_dml = read.table(file = "results/A17vsLSS_CHH_DMLS.txt", header = TRUE, sep = "\t", quote = "")
CHH_SUNNup = filter(CHH_dml,chr==chromosome & between(pos,(start-upstream),start))
CHH_SUNN = filter(CHH_dml,chr==chromosome & between(pos,start,end))
CHH_SUNNdown = filter(CHH_dml,chr==chromosome & between(pos,end,(end+downstream)))

CpG_dml = read.table(file = "results/A17vLSS_DSS_DML_CpG.txt", header = TRUE, sep = "\t", quote = "")
CpG_SUNNup = filter(CpG_dml,chr==chromosome & between(pos,(start-upstream),start))
CpG_SUNN = filter(CpG_dml,chr==chromosome & between(pos,start,end))
CpG_SUNNdown = filter(CpG_dml,chr==chromosome & between(pos,end,(end+downstream)))

CHG = rbind(select(CHG_SUNNup, c(chr,pos,diff)),select(CHG_SUNN, c(chr,pos,diff)),select(CHG_SUNNdown, c(chr,pos,diff)))
CHH = rbind(select(CHH_SUNNup, c(chr,pos,diff)),select(CHH_SUNN, c(chr,pos,diff)),select(CHH_SUNNdown, c(chr,pos,diff)))
CpG = rbind(select(CpG_SUNNup, c(chr,pos,diff)),select(CpG_SUNN, c(chr,pos,diff)),select(CpG_SUNNdown, c(chr,pos,diff)))

fnameCHG="results/CHG_DML_Medtr4g070970.bedgraph"
to_write=CHG[,c("chr","pos")]
to_write$stop=CHG$pos
to_write$score=CHG$diff
write.table(to_write,file=fnameCHG,quote=F,sep="\t",row.names = F,col.names = F)

fnameCHH="results/CHH_DML_Medtr4g070970.bedgraph"
to_write=CHH[,c("chr","pos")]
to_write$stop=CHH$pos
to_write$score=CHH$diff
write.table(to_write,file=fnameCHH,quote=F,sep="\t",row.names = F,col.names = F)

fnameCpG="results/CpG_DML_Medtr4g070970.bedgraph"
to_write=CpG[,c("chr","pos")]
to_write$stop=CpG$pos
to_write$score=CpG$diff
write.table(to_write,file=fnameCpG,quote=F,sep="\t",row.names = F,col.names = F)

if(length(CHG_SUNNup$diff!=0)) plot(CHG_SUNNup$diff)
if(length(CHH_SUNNup$diff!=0))plot(CHH_SUNNup$diff)
if(length(CpG_SUNNup$diff!=0))plot(CpG_SUNNup$diff)

if(length(CHG_SUNN$diff!=0))plot(CHG_SUNN$diff)
if(length(CHH_SUNN$diff!=0))plot(CHH_SUNN$diff)
if(length(CpG_SUNN$diff!=0))plot(CpG_SUNN$diff)

if(length(CHG_SUNNdown$diff!=0))plot(CHG_SUNNdown$diff)
if(length(CHH_SUNNdown$diff!=0))plot(CHH_SUNNdown$diff)
if(length(CpG_SUNNdown$diff!=0))plot(CpG_SUNNdown$diff)


```